import {s} from "../support/utils"

Cypress.Commands.add("assertIfLoggedIn", username => {
  cy.contains("Your Feed").should("have.class", "active")
  cy.hash().should("eq", "")
  cy.get(s("articles")).should("contain", "No articles are here")
  cy.get(s("logged-user")).should("contain", username)
})
