import { s } from "../support/utils"

Cypress.Commands.add("login", ({ email, password }) => {
  cy.visit("/login")
  cy.get(s("email")).type(email)
  cy.get(s("password")).type(password)
  cy.get(s("sign-in")).click()
})

Cypress.Commands.add("register", ({ username, email, password }) => {
  cy.visit("/register")
  cy.get(s("username")).type(username)
  cy.get(s("email")).type(email)
  cy.get(s("password")).type(password)
  cy.get(s("sign-up")).click()
})
