Cypress.Commands.add("registerRequest", user => {
  cy.request({
    url: "http://test-backend.koncikowski.pl/api/users",
    method: "POST",
    body: { user: { ...user } }
  })
})
