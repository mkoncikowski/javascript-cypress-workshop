import faker from "faker"

export const userBuilder = (username, email, password) => ({
  username: username ? username : faker.name.firstName().toLowerCase() + faker.random.number(),
  email: email ? email : faker.internet.email().toLowerCase(),
  password: password ? password : "password"
})