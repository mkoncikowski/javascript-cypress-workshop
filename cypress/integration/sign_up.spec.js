import { s } from "../support/utils"
import { userBuilder } from "../builders/user"

context("sign up", () => {
  it("should successfully create an account using real request", function() {
    const user = userBuilder()

    cy.register(user)
    cy.assertIfLoggedIn(user.username)
  })

  it("should successfully create an account with stubbed response", () => {
    const user = userBuilder()

    cy.server()
    cy.route({
      url: "http://test-backend.koncikowski.pl/api/users",
      method: "POST",
      response: {
        user: {
          username: user.username,
          email: user.email,
          token:
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOTM3YTM0MTQ5NzFlMDAxNGMzNGM5NiIsInVzZXJuYW1lIjoibHlkYTM5MjM4IiwiZXhwIjoxNTQxNTc1NzMyLCJpYXQiOjE1MzYzOTE3MzJ9.qC3hbLx1_4epFsaNOl1FRT0m4qYwHnaJO0btZC9ZpO8"
        }
      }
    }).as("createUser")

    cy.route({
      url: "http://test-backend.koncikowski.pl/api/articles/feed*",
      method: "GET",
      response: {articles: [], articlesCount: 0}
    }).as("getArticles")

    cy.register(user)
    cy.wait("@createUser")
    cy.wait("@getArticles")
    cy.assertIfLoggedIn(user.username)
  })

  it("should display an error about duplicated user", () => {
    const user = userBuilder()

    cy.registerRequest(user)
    cy.register(user)
    cy.get(".error-messages").should("contain", "username is already taken")
    cy.get(".error-messages").should("contain", "email is already taken")
  })

  it("should display an error message about dublicated user with stubbed response", () => {
    const user = userBuilder()

    cy.server()
    cy.route({
      url: "http://test-backend.koncikowski.pl/api/users",
      method: "POST",
      status: 422,
      response: {
        errors: {
          username: "is already taken.",
          email: "is already taken."
        }
      }
    }).as("createUser")
    cy.register(user)
    cy.wait("@createUser")
    cy.get(".error-messages").should("contain", "username is already taken")
    cy.get(".error-messages").should("contain", "email is already taken")
  })
})
