import { s } from "../support/utils";
import {userBuilder} from "../builders/user" 

context("sign in", () => {
  it("should be able to sign in with valid credentials", () => {
    const user = userBuilder();

    cy.registerRequest(user)
    cy.login(user);
    cy.assertIfLoggedIn(user.username)
  });
});
